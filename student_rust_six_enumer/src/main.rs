// 仓库地址  https://gitee.com/fengliuwt/rust_student.git

enum  IpAddrKind{
    V4,
    V6,
}
//枚举和结构体 struct 的结合
struct IpAddrEnum {
    kind:IpAddrKind,
    address:String,
}

//枚举允许我们直接将其关联的数据嵌入枚举变体内
enum  IpAddrStr{
    V4(String),
    V6(String),
}

//枚举每个变体可以拥有不同类型和数量的关联数据,
enum IpAddr{
    V4(u8,u8,u8,u8),
    V6(String),
}
//
struct IPFourAddr{
    ip:(u8,u8,u8,u8),
    port:(u32),
}

struct IPSixAddr{
    ip:(String),
    port:(u32),
}
enum  StructIP{
    V4(IPFourAddr),
    V6(IPSixAddr)
}


//枚举内嵌不同数据的变体
enum Message{
    Quit,
    Move{x:i32,y:i32},
    Write(String),
    ChangeColor(i32,i32,i32),
}
impl Message{
    fn call(&self){

    }
}

fn main() {
    println!("Hello, world!");

    //枚举值
    //枚举的变体全都位于其标识符的命名空间中，并使用两个冒号来将标识符和变体分隔开来。
    let four =IpAddrKind::V4;
    let six=IpAddrKind::V6;
    //函数调用枚举
    let four_fn=route(IpAddrKind::V4);
    let six_fn=route(IpAddrKind::V6);

    //枚举和结构体的结合使用
    let four_struct=IpAddrEnum{
        kind:IpAddrKind::V4,
        address:String::from("127.0.0.1")
    };
    let six_struct=IpAddrEnum{
        kind:IpAddrKind::V6,
        address:String::from("::1")
    };

    //枚举赋值
    let home=IpAddrStr::V6(String::from("192.168.1.120"));
    let loopback=IpAddrStr::V6(String::from("::2"));
    //枚举赋值不同类型,多个参数
    let host_four=IpAddr::V4(127,0,0,1);
    let host_six=IpAddr::V6(String::from("::3"));

    let message=Message::Write(String::from("red"));
    message.call();

    //option Option枚举及其在空值处理方面的优势
    //  option 枚供了一个拥有类似概念的枚举，我们可以用它来标识一个值无效或缺失。这个枚举就是Option<T>，它在标准库中被定义为如下所示的样子：
    //enum Option<T>{
    //    Some(T),
    //       None,
    // }
    let some_number=Some(5);
    let some_string=Some("hello");
    let lat_number:Option<i32>=None;


    let x:i8=5;
    let y:Option<i8>=Some(1);
    let y=23;
    let yx=y+x;
    println!("{}",yx);


}

fn route(ip_type:IpAddrKind)->IpAddrKind{
    ip_type
}
