// 仓库地址  https://gitee.com/fengliuwt/rust_student.git


/**
       控制流运算符match   match，它允许将一个值与一系列的模式相比较，并根据匹配的模式执行相应代码
       \将match与枚举相结合在许多情形下都是非常有用的

       匹配必须穷举所有的可能   如果范围太大可以匹配自己想要的结果  其余的值 使用  下划线 _ 作为通配符 示例 第35行



       在本章中，我们学会了如何使用枚举来创建自定义类型，它可以包含一系列可被列举的值。
       我们同时也展示了如何使用标准库中的Option<T>类型，以及它会如何帮助我们利用类型系统去避免错误。
       当枚举中包含数据时，我们可以使用match或if let来抽取并使用这些值。具体应该使用哪个工具则取决于我们想要处理的情形有多少。
 */

#[derive(Debug)]
enum  UsState{
    Alabama,
    Alaska,
}

enum Coin{
    Penny,
    Nickel,
    Dime,
    Quarter(UsState),
}

fn main() {

    println!("Hello, world!");

    let test_match=value_in_cents(Coin::Quarter(UsState::Alabama));
    print!("{}",test_match);


    let six=Some(6);
    let six_match_option=plus_one(six);
    let none=plus_one(None);

    let some_u8=plus_two(7);
    println!("{}",some_u8);


    //简单控制流  if let

    let some_u8_value=Some(0u8);
    match some_u8_value{
        Some(3)=>println!("three"),
        _=>println!("four"),
    }
    //这里的if let语法使用一对以=隔开的模式与表达式。它们所起的作用与match中的完全相同，表达式对应match中的输入，而模式则对应第一个分支。
    if let Some(3)=some_u8_value{
        println!("three");
    }

    // if let  可以搭配 else {  }
    let mut count=0;
    let coin=Coin::Quarter(UsState::Alabama);

    match coin{
        Coin::Quarter(state)=>println!("state form {:#?}",state),
        _=>count+=1,
    }
    println!("{}",count);
    //
    let coin1=Coin::Quarter(UsState::Alaska);
    if let Coin::Quarter(state)=coin1{
        println!("state form {:?}",state);
    }else {
        count+=1;
    }
    println!("{}",count);

////[source.ustc]
// registry = "https://mirrors.ustc.edu.cn/crates.io-index"
// registry = "git://mirrors.ustc.edu.cn/crates.io-index"


}


fn  value_in_cents(coin:Coin)->u32{
    match coin {
        Coin::Penny=>{
        println!("Lucky penny!!");
           return  1
        },
        Coin::Nickel=>5,
        Coin::Dime=>10,
        Coin::Quarter(state)=>{

            println!("State from {:#?}",state);
            25
        },
    }
}

fn plus_one(x:Option<i32>)->Option<i32>{
    match x {
        None=>None,
        Some(i)=>Some(i+1),
    }
}

fn plus_two(some_u8:u8)->& 'static str{
    match some_u8 {
        1=>{println!("one"); "one"},
        3=>{println!("three"); "three"},
        5=>{println!("five"); "five"},
        7=>{println!("seven"); "seven"},
        _=>{println!("Zero"); "Zero"},
    }
}


