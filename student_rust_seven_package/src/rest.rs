mod  test{
    pub mod hosting{
        pub fn add_to_string(){

        }
    }
}

pub fn  eat_at_restaurant(){
    //绝对路径
    crate::test::hosting::add_to_string();
    //相对路径
    test::hosting::add_to_string();
}

// 使用super关键字开始构造相对路径
// 考虑到back_of_house模块与serve_order函数联系较为紧密，当我们需要重新组织单元包的模块树时应该会同时移动它们，所以本例使用了super。

fn serve_order(){

}

mod  black_of_house{
    fn fix_order(){
        cook_order();
        super::serve_order();
    }
    fn cook_order(){}
}

