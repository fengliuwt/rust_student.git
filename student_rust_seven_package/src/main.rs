use rand::Rng;

mod rest;

// 仓库地址  https://gitee.com/fengliuwt/rust_student.git
/**
      第七章 :::   使用包、单元包及模块来管理日渐复杂的项目

      一个包（package）可以拥有多个二进制单元包及一个可选的库单元包。而随着包内代码规模的增长，你还可以将部分代码拆分到独立的单元包（crate）中，并将它作为外部依赖进行引用。

      • 包（package）：一个用于构建、测试并分享单元包的Cargo功能。
      • 单元包（crate）：一个用于生成库或可执行文件的树形模块结构。
      • 模块（module）及use关键字：它们被用于控制文件结构、作用域及路径的私有性。
      • 路径（path）：一种用于命名条目的方法，这些条目包括结构体、函数和模块等。


      用于在模块树中指明条目的路径
      路径有两种形式：
      • 使用单元包名或字面量crate从根节点开始的绝对路径。
      • 使用self、super或内部标识符从当前模块开始的相对路径。

      使用pub关键字来暴露路径

      用use关键字将路径导入作用域     基于路径来调用函数的写法看上去会有些重复与冗长。
      使用as关键字来提供新的名称   使用use将同名类型引入作用域时所产生的问题还有另外一种解决办法：我们可以在路径后使用as关键字为类型指定一个新的本地名称，也就是别名

      使用pub use重导出名称当我们使用use关键字将名称引入作用域时，这个名称会以私有的方式在新的作用域中生效。


      使用外部包   ，我们需要在Cargo.toml中添加下面的内容：
      [dependencies]
      rand="0.7"


      注意，标准库（std）实际上也同样被视作当前项目的外部包
      例如，我们可以通过如下所示的语句来引入HashMap： use  std::collections::HashMap;

      使用嵌套的路径来清理众多use语句:   use  std::cmp::Ordering;  use  std::io;

      我们还可以在同一行内使用嵌套路径来将上述条目引入作用域。
      这一方法需要我们首先指定路径的相同部分，再在后面跟上两个冒号， use  std::{cmp::Ordering,io}

      use std::io;
      use std::io::Write;
      这两条路径拥有共同的std::io前缀，该前缀还是第一条路径本身。为了将这两条路径合并至一行use语句中，我们可以在嵌套路径中使用self，
      use std::io::{self,Write};    此语句会将std::io与std::io::Write引入作用域。

      通配符   *
      use  std::collections::*;    这行use语句会将定义在std::collections内的所有公共条目都导入当前作用域


      将模块拆分为不同的文件
      当所有的示例都被定义于同一文件内的不同模块中。当模块规模逐渐增大时，我们可以将它们的定义移动至新的文件，从而使代码更加易于浏览。

      总结
      Rust允许你将包拆分为不同的单元包，并将单元包拆分为不同的模块，从而使你能够在其他模块中引用某个特定模块内定义的条目。
      为了引用外部条目，你需要指定它们的绝对路径或相对路径。我们可以通过use语句将这些路径引入作用域，接着在该作用域中使用较短的路径来多次使用对应的条目。
      模块中的代码是默认私有的，但你可以通过添加pub关键字来将定义声明为公共的。


*/

fn main() {
    println!("Hello, world!");

    let secret_number= rand::thread_rng().gen_range(1,101);



}
