use std::panic::resume_unwind;

fn main() {
    println!("Hello, world!");

    // Rust提供了3种循环：loop、while和for。下面让我们来逐一看下
    // loop{
    //     println!("123")    //这样loop程序会一直打印123字段
    // }

    //lop循环的停止判断
    let mut blue =5;
    let result= loop {
        blue+=1;
        if blue==10 {
           break blue*2;
        }
    };
    println!("{}",result);
    //while循环的方法
    let ac=while_test();
    println!("{}",ac);

    //for 循环的方法
    for_test();

    for  number in (1..4).rev() {
        println!("循环{}次",number);
    }
    println!("循环结束")
}


fn while_test()->i32{
    let a=[0,1,2,3,4,5];
    let mut  index=0;
    while  index<5{
        println!("a数组中的值:{}",a[index]);
        index=index+1;
    }
    return a[2];
}

fn  for_test(){
    let a=[6,7,8,9,10];
    for _eleen in a.iter() {
        println!("for_test 中 a数组的值:{}",_eleen)
    }
}
