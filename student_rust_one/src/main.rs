

fn main() {
    let feibo= fei_bo_na_qi(5);

    let bo_digui = fei_bo_digui(5);

    let jiecheng = digui_jiecheng(10);
    println!("{}",feibo);
    println!("{}",bo_digui);
    println!("{}",jiecheng);
    println!("Hello, world!");

    println!("gitee.com")

}


//for循环实现斐波那契
fn fei_bo_na_qi(_n:i32) ->i32{
    if _n<1{
        return -1;
    }else if _n==1||_n==2 {
        return 1;
    }
    let mut a=1;
    let mut b=1;
    let mut c=0;
    for _i in 0.._n-2 {
        c=a+b;
        a=b;
        b=c;
    }
    return c
}
//递归实现斐波那契数列
fn  fei_bo_digui(_n:i32)->i32{
    if _n==1||_n==2 {
        return 1;
    }
    if _n>2 {
        return fei_bo_digui(_n-1)+fei_bo_digui(_n-2);
    }
    return -1;
}
//使用递归算阶乘
fn digui_jiecheng(_n:i32)->i32{
    if _n<=0 {
        return _n;
    }else if _n==1 {
        return 1;
    }else {
        return _n*digui_jiecheng(_n-1);
    }

}
