use std::panic::resume_unwind;

/**
                        下面是第五章        讲解    方法
                        方法与函数十分相似：它们都使用fn关键字及一个名称来进行声明；它们都可以拥有参数和返回值；
                        另外，它们都包含了一段在调用时执行的代码。但是，方法与函数依然是两个不同的概念，
                        因为方法总是被定义在某个结构体（或者枚举类型、trait对象，我们会在第6章和第17章分别介绍它们）的上下文中，
                        并且它们的第一个参数永远都是self，用于指代调用该方法的结构体实例。
*/

//结构体
#[derive(Debug)]
struct Rectangle{
    width:u32,
    height:u32,
}
//定义在Rectangle结构体中的方法
impl Rectangle {
    fn area(&self)->u32{
        self.height*self.width
    }
    fn can_old(&self,other:&Rectangle)->bool{
        self.width>other.width&&self.height>other.height
    }
}

//关联函数使用 双冒号 调用 ::
impl Rectangle {
    fn square(size:u32,text:u32)->Rectangle{
        Rectangle{width:size,height:text}
    }
}

/**
      分割线

      //可以定义多个impl
impl Rectangle {
    fn area(&self)->u32{
        self.height*self.width
    }
}
impl Rectangle {
    fn can_old(&self,other:&Rectangle)->bool{
        self.width>other.width&&self.height>other.height
    }
}
*/





fn main() {
    println!("Hello, world!");

    //初始化结构体
    let rect1=Rectangle{
        width:20,
        height:40,
    };
    let rect2=rect1.area();
    println!("{}",rect2);
    let rect3=Rectangle{
        width:10,
        height:20,
    };
    let rect4=Rectangle{
        width:40,
        height:60,
    };

    println!("{}",rect1.can_old(&rect3));
    println!("{}",rect1.can_old(&rect4));

    //关联函数使用 双冒号 调用 ::
    let rect5=Rectangle::square(32,50);
    eprintln!("{:#?}",rect5);


}
