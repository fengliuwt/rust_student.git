/**
                                             第八章  通用集合类型
           Rust标准库包含了一系列非常有用的被称为集合的数据结构。大部分的数据结构都代表着某个特定的值，但集合却可以包含多个值。
           与内置的数组与元组类型不同，这些集合将自己持有的数据存储在了堆上。这意味着数据的大小不需要在编译时确定，
           并且可以随着程序的运行按需扩大或缩小数据占用的空间。不同的集合类型有着不同的性能特性与开销，
           需要学会如何为特定的场景选择合适的集合类型.

           被广泛使用在Rust程序中的三种集合:
           动态数组: vector  可以让你连续的存储任意多个值;
           字符串 :  string  是字符的集合,
           哈希映射:  HashMap 可以让你将值关联到一个特定的键上，它是另外一种数据结构—映射（map）的特殊实现。

*/
fn main() {
    println!("Hello, world!");

    //使用动态数组存储多个值
    //1.创建一个i32数据的空动态数组;
    let v:Vec<i32>=Vec::new();
    //rust语言提供了一个用于简化代码的vec! 宏。  vec!可以帮助我们创建一个新的动态数组;
    let v=vec![1,2,3];
    //2.更新动态数组
    let mut v=Vec::new();   //添加mut 关键字表示数组可变,
    v.push(5); //v.push()  向动态数组中添加值;
    v.push(6);
    v.push(7);
    v.push(8);
    v.push(9);

    //3.销毁动态数组时也会销毁其中的元素
    /**
    {
       let v=vec![1,2,3];
       //在这里执行 数组相关的操作;
    } <-  这里数组v 离开作用域并随之销毁;
    */

    //4.读取动态数组中的元素
    let v23=vec![1,2,3];
    let third=&v23[2];
    println!("{}",third);
    match v23.get(2) {
        Some(third)=>println!("{}",third),
        None=>println!("There is not third element"),
    }
   // let does_not_exist=&v23[100];   //，[]方法会因为索引指向了不存在的元素而导致程序触发panic。
    //let does_not_exist=v23.get(100);  //get方法会在检测到索引越界时简单地返回None，而不是使程序直接崩溃。

    let mut v24=vec![1,2,3,4,5];
    let first=&v24[0];
    // v24.push(6);   //此处会报错,错误是由动态数组的工作原理导致的：动态数组中的元素是连续存储的，插入新的元素后也许会没有足够多的空间将所有元素依次相邻地放下，
                           // 这就需要分配新的内存空间，并将旧的元素移动到新的空间上。
    println!("{}",first);

    //5.遍历动态数组
    let v33=vec![11,12,13,14,15];
    for  i in v33  {
        println!("{}",i);
    }
    //5.1我们同样也可以遍历可变的动态数组，获得元素的可变引用，并修改其中的值。
    let mut v34=vec![16,17,18,19,20];
    for i in &mut v34 {
        *i+=33;    //为了使用+=运算符来修改可变引用指向的值，我们首先需要使用解引用运算符（*）来获得i绑定的值。
        println!("{}",i);
    }

    //6.使用枚举来存储多个类型的值
    let enum_vec=vec![
        SpreadsheetCell::Int(8),
        SpreadsheetCell::Float(0.7),
        SpreadsheetCell::Text(String::from("Sorry"))
    ];

}
enum SpreadsheetCell{
    Int(i32),
    Float(f64),
    Text(String),
}
